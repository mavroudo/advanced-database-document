FROM python:3.8-slim-buster
COPY python_code/ /python_code
COPY requirements.txt /requirements.txt
RUN pip3 install -r /requirements.txt
EXPOSE 5000
RUN cd /
CMD ["gunicorn", "--bind", "0.0.0.0:5000", "python_code.app:app"]