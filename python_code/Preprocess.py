from collections import Counter

import nltk


class Preprocess:
    def __init__(self):
        print("> Downloading natural language packages...")
        nltk.download('punkt')
        nltk.download('stopwords')
        nltk.download('wordnet')
        self.stop_words = set(nltk.corpus.stopwords.words("english"))

    def BOW_document(self, text: str):
        tokens = [(word[0].lower(), word[1]) for word in self._spans(text) if word[0] not in self.stop_words]
        lem = nltk.stem.wordnet.WordNetLemmatizer()
        tokensStemmed = [(lem.lemmatize(word[0]), word[1]) for word in tokens]  # Stemming
        tokensStemmed.sort(key=lambda x: x[0])
        if not tokensStemmed:  # In case a dictionary is empty
            return {}
        nftd = self._calculate_nftd(self._group_same(tokensStemmed))
        return nftd

    def _spans(self, txt: str):
        tokenizer = nltk.RegexpTokenizer(r'\w+')  # get the words
        tokens = tokenizer.tokenize(txt)
        offset = 0
        for token in tokens:
            offset = txt.find(token, offset)
            yield token, offset, offset + len(token)
            offset += len(token)

    def _group_same(self, tokensStemmed):
        dictionary = {}
        for index, word in enumerate(tokensStemmed):
            if word[0] in dictionary:
                dictionary[word[0]].append(word[1])
            else:
                dictionary[word[0]] = [word[1]]
        return dictionary

    def _calculate_nftd(self, dictionary):
        maxftd = max([len(dictionary[i]) for i in dictionary])
        return {word: {"positions": dictionary[word], "nftd": len(dictionary[word]) / maxftd} for word in dictionary}
