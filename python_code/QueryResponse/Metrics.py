import math


class Metrics:
    def __init__(self):
        pass

    def cosine(self, query_vector, record_vector):
        ql = math.sqrt(sum([math.pow(query_vector[x]["nftd"], 2) for x in query_vector]))
        cl = math.sqrt(sum([math.pow(record_vector[x], 2) for x in record_vector]))
        cq = sum([record_vector[x] * query_vector[x]["nftd"] for x in record_vector])
        return cq / (cl * ql)

    def jaccard(self, query_vector, record_vector):
        cq = sum([record_vector[x] * query_vector[x]["nftd"] for x in record_vector])
        c_2 = sum([math.pow(record_vector[x], 2) for x in record_vector])
        q_2 = sum([math.pow(query_vector[x]["nftd"], 2) for x in query_vector])
        return cq / (c_2 + q_2 - cq)

    def tanimoto(self, query_vector, record_vector):
        cq = sum(
            [max(record_vector[x], query_vector[x]["nftd"]) - min(record_vector[x], query_vector[x]["nftd"]) for x in
             record_vector])
        mcq = sum([max(record_vector[x], query_vector[x]["nftd"]) for x in record_vector])
        return cq / mcq

    def matusita(self, query_vector, record_vector):
        cq = sum([math.pow(math.sqrt(record_vector[x]) - math.sqrt(query_vector[x]["nftd"]), 2) for x in record_vector])
        return math.sqrt(cq)

    def pearson(self, query_vector, record_vector):
        return sum(
            [math.pow(record_vector[x] - query_vector[x]["nftd"], 2) / query_vector[x]["nftd"] for x in record_vector])
