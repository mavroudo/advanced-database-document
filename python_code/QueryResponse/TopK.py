from python_code.MongoDatabase.MongoDatabase import MongoDatabase
from python_code.Preprocess import Preprocess
from python_code.QueryResponse.Metrics import Metrics
from python_code.MongoDatabase.MongoDBInvertedIndex import MongoDBInvertedIndex


class TopK:

    def __init__(self, mongodb: MongoDatabase):
        self.metrics = Metrics()
        self.preprocess = Preprocess()
        self.mongodb = mongodb
        self.mongo_inverted = self.mongodb.invertedIndexDB

    def topk_query(self, query):
        m = self.preprocess.BOW_document(query["query"])
        words = [i for i in m]
        l = self.mongo_inverted.getRelevantDocs(words)
        resp =self._topk_query(int(query["topk"]),query["metric"],m,l)
        return [self.mongodb.documentDB.find_document_by_id(document[0]) for document in resp],words

    def _topk_query(self, k: int, metric: str, query_vector, relevant_docs):
        switcher = {"cosine": self.metrics.cosine, "jaccard": self.metrics.jaccard, "tanimoto": self.metrics.tanimoto,
                    "matusita": self.metrics.matusita, "pearson": self.metrics.pearson}
        func = switcher.get(metric, lambda: self.metrics.cosine)
        scores = [(doc, func(query_vector, relevant_docs[doc])) for doc in relevant_docs]
        if metric in ["tanimoto", "pearson", "matusita"]:
            scores.sort(key=lambda x: x[1], reverse=False)
        else:
            scores.sort(key=lambda x: x[1], reverse=True)
        return scores[:k]

    def create_content(self,document,queried_words):
        phrases=[]
        doc=document["doc_actual"]
        for word in queried_words:
            if word in document["bow"]:
                positions = document["bow"][word]
                for p in positions:
                    prev_dot=max(0,p-20)
                    next_dot=min(p+len(word)+20,len(doc))
                    for i in range(p,prev_dot,-1):
                        if doc[i]=='.':
                            prev_dot=i
                            break
                    for j in range(p+len(word)+1,next_dot):
                        if doc[j]=='.':
                            next_dot=j
                            break
                    s= doc[prev_dot+1:p-1]+" <b>"+doc[p:p+len(word)]+"</b> "+doc[p+len(word)+1:next_dot+1]
                    phrases.append(s)
        document["preview"]="...".join(phrases)
        return document
