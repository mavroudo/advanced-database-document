import json
from flask import Flask, render_template, request, redirect
from python_code.MongoDatabase.MongoDatabase import MongoDatabase
from python_code.QueryResponse.TopK import TopK

host = "mongodb"
port = 27017
mongodb = MongoDatabase(host, port)
topk = TopK(mongodb)
data = []
# print("Initializing database, this might take some time!!")
# with open("input_documents/combined.json", "r") as json_file:
#     for line in json_file:
#         data.append(json.loads(line))
# Drop and initialize the database the first time it is executed
# mongodb.drop_db()
# mongodb.initialiaze_db(data)
# print("Database is ready")
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        query_content = request.form
        resp,words = topk.topk_query(query_content)
        if resp:
            docs=[topk.create_content(r,words) for r in resp]
            return render_template('index.html',documents=docs)
    return render_template('index.html')

@app.route('/content/<string:id>')
def view_content(id):
    document = mongodb.documentDB.find_document_by_id(id)
    return render_template('content.html',document=document)

@app.route('/add/page')
def add_document_page():
    return render_template('add_document.html')

@app.route('/add',methods=['POST'])
def add_document():
    document=request.form
    mongodb.add_document(document)
    return redirect('/')

@app.route('/delete/<string:id>',methods=['GET'])
def delete_document(id):
    mongodb.remove_document(id)
    return redirect('/')

if __name__ == '__main__':
    app.run(debug=True)
