from pymongo import MongoClient

from python_code.MongoDatabase.MongoDBInvertedIndex import MongoDBInvertedIndex
from python_code.Preprocess import Preprocess
from python_code.MongoDatabase.MongoDBDocuments import MongoDBDocuments


class MongoDatabase:

    def __init__(self, host: str, port: int):
        self.client = MongoClient(host=host, port=port)
        self.db = self.client.documents
        self.documentDB = MongoDBDocuments(self.db)
        self.invertedIndexDB = MongoDBInvertedIndex(self.db)
        self.preprocess = Preprocess()

    def drop_db(self):
        self.db['documents'].drop()
        self.db['metadata'].drop()
        self.db['inverted_index'].drop()

    def add_document(self, document):
        oid, inv_ind = self._preprocess_doc(document)
        self.invertedIndexDB.addDocument(inv_ind, oid)
        return str(oid)

    def remove_document(self, oid):
        doc = self.documentDB.find_document_by_id(str(oid))
        self.documentDB.delete_document_by_id(str(oid))
        self.invertedIndexDB.removeDocument(doc)

    def _preprocess_doc(self, doc):
        preprocessed = self.preprocess.BOW_document(doc["contents"])
        bow = {word: preprocessed[word]["positions"] for word in preprocessed}
        inv_ind = {word: preprocessed[word]["nftd"] for word in preprocessed}
        record = {'title': doc['title'], "doc_actual": doc['contents'], "bow": bow}
        return self.documentDB.insert_document(record), inv_ind

    def initialiaze_db(self, documents):
        inverted_index = {}
        total_size = len(documents)
        self.db['metadata'].insert_one({"nt": total_size})
        for doc in documents:
            oid, inv_ind = self._preprocess_doc(doc)
            for word in inv_ind:
                if word not in inverted_index:
                    inverted_index[word] = []
                inverted_index[word].append((str(oid), inv_ind[word]))
        newlist = [{"word": word, "list": inverted_index[word], "length": len(inverted_index[word])} for word in
                   inverted_index]
        self.invertedIndexDB.initializeInvertedIndex(newlist)

    def update_document(self, oid: str, doc):
        preprocessed = self.preprocess.BOW_document(doc["contents"])
        bow = {word: preprocessed[word]["positions"] for word in preprocessed}
        inv_ind = {word: preprocessed[word]["nftd"] for word in preprocessed}
        record = {'title': doc['title'], "doc_actual": doc['contents'], "bow": bow}
        self.documentDB.update_document_by_id(oid, record)
        retrieved = self.documentDB.find_document_by_id(oid)
        self.invertedIndexDB.updateDocument(oid, retrieved, inv_ind)
