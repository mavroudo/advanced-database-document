import json

from bson.objectid import ObjectId

from python_code.Preprocess import Preprocess


class MongoDBInvertedIndex:

    def __init__(self, db):
        self.collection = db['inverted_index']
        self.preprocess = Preprocess()

    def initializeInvertedIndex(self, inverted_index):
        return self.collection.insert_many(inverted_index)

    def addDocument(self, inv_ind, oid):
        words = [w for w in inv_ind]
        relevant = list(self.collection.find({"word": {"$in": words}}))
        counter = 0
        for index, word in enumerate(words):
            if counter >= len(relevant):  # add the rest of them
                for w in words[counter:]:
                    self.collection.insert({"word": w, "list": [(str(oid), inv_ind[w])], "length": 1})
                break
            if relevant[counter]["word"] == word:  # exists
                relevant[counter]["list"].append((str(oid), inv_ind[word]))
                relevant[counter]["length"] += 1
                self.collection.replace_one({"word": word}, relevant[counter])
            else:  # doesnt exist
                self.collection.insert({"word": word, "list": [(str(oid), inv_ind[word])], "length": 1})
            counter += 1

    def getRelevantDocs(self, listOfQueryingWords):
        docs = {}
        for word in listOfQueryingWords:
            l = list(self.collection.find({"word": word}))
            if l:
                for doc in l[0]["list"]:
                    if doc[0] not in docs:
                        docs[doc[0]] = {}
                    docs[doc[0]][word] = doc[1]
        return docs

    def removeDocument(self, doc_record):
        words = [w for w in doc_record["bow"]]
        relevant = list(self.collection.find({"word": {"$in": words}}))
        for record in relevant:
            keep_in = []
            for element in record["list"]:
                if element[0] != str(doc_record["_id"]):
                    keep_in.append(element)
            if len(keep_in) != 0:
                record["list"] = keep_in
                record["length"] -= 1
                self.collection.replace_one({"_id": record["_id"]}, record)
            else:
                self.collection.delete_one({"_id": record["_id"]})

    def updateDocument(self, oid: str, record, inv_ind):
        self.removeDocument(record)
        self.addDocument(inv_ind, oid)
