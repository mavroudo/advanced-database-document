import json

from bson.objectid import ObjectId

from python_code.Preprocess import Preprocess


class MongoDBDocuments:

    def __init__(self, db):
        self.collection = db['documents']
        self.preprocess = Preprocess()

    def insert_document(self, record: dict):
        result = self.collection.insert_one(record)
        return result.inserted_id

    def find_document_by_id(self, id: str):
        return self.collection.find_one({"_id": ObjectId(id)})

    def delete_document_by_id(self, id: str):
        return self.collection.delete_one({"_id": ObjectId(id)})

    def update_document_by_id(self, id: str, record: dict):
        self.collection.replace_one({"_id": ObjectId(id)}, record)
